# Changelog

## Unreleased

### Added
- `alpha` extension for feature attribute builder
- PNG export

### Changed
- avoid drawing features with VisibleAttribute false
- Move SVG export to `features` and make it usable for maps as well

### Deprecated

### Removed

### Fixed
- Add alpha attribute comprehension for all standard features.
- Package name for SerializeableAttribute

### Security

## 0.3.0 - 2024-06-04

### Changed

- Package changed to `space.kscience`
- Kotlin 2.0

### Fixed

- Use of generated resources for Wasm
